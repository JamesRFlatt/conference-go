import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    resp = requests.get(url, headers=headers)

    return resp.json()["photos"][0]["url"]


def get_location_weather(city, state):
    # get lat and lon
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # get weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_response = response.json()

    try:
        temp = json_response["main"]["temp"]
        description = json_response["weather"][0]["description"]

        return {"temp": temp, "description": description}
    except (KeyError, IndexError):
        return None
