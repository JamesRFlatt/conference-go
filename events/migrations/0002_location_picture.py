# Generated by Django 4.0.3 on 2023-02-10 00:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='picture',
            field=models.URLField(default=''),
        ),
    ]
